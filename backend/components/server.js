const express = require("express");
const bodyParser = require("body-parser");
const MongoClient = require("mongodb").MongoClient;
const assert = require("assert");
const passport = require("passport");

const users = require("../routes/api/users");

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());

const PORT = process.env.PORT || 5000;

const url = "mongodb://localhost:27017";

const dbName = "dbUsers";

const client = new MongoClient(url);

client.connect((error) => {
	assert.equal(error, null);
	console.log("Connected correctly to server.");

	const db = client.db(dbName);

	client.close();
});

app.use(passport.initialize());

require("../config/passport")(passport);

app.use("/api/users", users);

app.listen(PORT, () => {
	console.log(`Server listening on port ${PORT}`);
});

app.get("/", (req, res) => {
	res.json({ message: "Welcome to MERN stack development" });
});
