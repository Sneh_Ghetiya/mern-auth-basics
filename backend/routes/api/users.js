const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const keys = require("../../config/keys");

const validateRegisterInput = require("../../validation/Register");
const validateLoginInput = require("../../validation/Login");

const User = require("../../models/User");

router.post("/register", (req, res) => {
	const { errors, isValid } = validateRegisterInput(req.body);

	if (!isValid) {
		res.status(400).json({ errors });
	}

	User.findOne({ email: req.body.email }).then((user) => {
		if (user) {
			return res
				.status(400)
				.json({ errors: { email: "Email already exists" } });
		} else {
			const newUser = new User({
				name: req.body.name,
				email: req.body.email,
				password: req.body.password,
			});

			bcrypt.genSalt(10, (err, salt) => {
				bcrypt.hash(newUser.password, salt, (err, hash) => {
					if (err) throw err;
					newUser.password = hash;
					newUser
						.save()
						.then((user) =>
							res.json(user).catch((err) => res.status(500).json(err))
						);
				});
			});
		}
	});
});

router.post("/login", (req, res) => {
	const { errors, isValid } = validateLoginInput(req.body);

	if (!isValid) {
		return res.status(400).json({ errors });
	}

	const { email, password } = req.body;

	User.findOne({ email }).then((user) => {
		if (!user) {
			return res.status(404).json({ errors: { email: "Email not found" } });
		}

		bcrypt.compare(password, user.password).then((isMatch) => {
			if (isMatch) {
				const payload = {
					id: user._id,
					name: user.name,
				};

				jwt.sign(payload, keys.secret, { expiresIn: 86400 }, (err, token) => {
					if (err) throw err;
					res.json({
						success: true,
						token: "Bearer " + token,
					});
				});
			} else {
				res.status(401).json({ errors: { password: "Incorrect password" } });
			}
		});
	});
});

module.exports = router;
