const Validator = require("validator");
const isEmpty = require("is-empty");

module.exports = function validateRegisterInput(data) {
	let errors = {};

	data.name = !isEmpty(data.name) ? data.name : "";
	data.email = !isEmpty(data.email) ? data.email : "";
	data.password = !isEmpty(data.password) ? data.password : "";
	data.password_confirmation = !isEmpty(data.password_confirmation)
		? data.password_confirmation
		: "";

	if (Validator.isEmpty(data.name)) errors.name = "Name is required";
	if (Validator.isEmpty(data.email)) {
		errors.email = "Email is required";
	} else if (!Validator.isEmail(data.email)) {
		errors.email = "Email is not valid";
	}
	if (Validator.isEmpty(data.password)) {
		errors.password = "Password is required";
	}
	if (Validator.isEmpty(data.password_confirmation)) {
		errors.password_confirmation = "Password confirmation is required";
	}
	if (!Validator.isLength(data.password, { min: 8, max: 20 })) {
		errors.password = "Password must be between 8 and 20 characters long";
	}
	if (!Validator.equals(data.password, data.password_confirmation)) {
		errors.password_confirmation = "Password confirmation must match password";
	}

	return {
		errors,
		isValid: isEmpty(errors),
	};
};
